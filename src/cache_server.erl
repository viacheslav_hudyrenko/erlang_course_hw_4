-module(cache_server).
-behavior(gen_server).

-export([start_link/1, stop/0, insert/2, lookup/1, lookup/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

start_link({ttl, Time_interval}) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [{ttl, Time_interval}], []).

init([{ttl, Time_interval}]) ->
    CacheState = ets:new(my_cache, [ordered_set, named_table]),
    _TimerStart = timer:start(),
    timer:apply_interval(timer:seconds(Time_interval), ets, delete, my_cache),
    {ok, CacheState}.

% ------------------------------------------------------------
% --------------------------- actions ------------------------

insert(Key, Value) ->
	gen_server:call(?MODULE, {insert, Key, Value}).

lookup(Key) ->
	gen_server:call(?MODULE, {lookup, Key}).

lookup(Start_time, End_time) ->
	gen_server:call(?MODULE, {lookup, Start_time, End_time}).

stop() -> 
    gen_server:call(?MODULE, stop).

% ------------------------------------------------------------
% ---------------------------- handlers ----------------------

handle_call({insert, Key, Value}, _From, State) ->
	{reply, ets_time:insert(my_cache, {Key, Value}), State};

handle_call({lookup, Key}, _From, State) ->
	{reply, ets:lookup(my_cache, Key), State};

handle_call({lookup, Start_time, End_time}, _From, State) ->
	{reply, ets_time:lookup(my_cache, Start_time, End_time), State};

handle_call(stop, _From, _State) ->
    {stop, ets:delete(my_cache), stopped}.

handle_cast(_Msg, State) -> 
    {noreply, State}.

handle_info(_Info, State) -> 
    {noreply, State}.

terminate(_Reason, _State) -> 
    ok.

code_change(_OldVsn, State, _Extra) -> 
    {ok, State}.

